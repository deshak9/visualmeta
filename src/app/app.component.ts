﻿import {Component} from '@angular/core';
import {UserService} from "./_services/user.service";

@Component({
  selector: 'app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {
  constructor(public us: UserService) {

  }
}