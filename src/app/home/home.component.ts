﻿import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';

import {Post, User} from '../_models';
import {UserService} from '../_services';
import {PostService} from "../_services/post.service";
import {AlertService} from "../_services/alert.service";

@Component({templateUrl: 'home.component.html', styleUrls: ['home.component.scss']})
export class HomeComponent implements OnInit {
  currentUser: User;
  users: User[] = [];
  posts: Post[] = [];
  loading = false;

  constructor(private userService: UserService, private postService: PostService, private alertService: AlertService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.loadPosts();
  }

  loadPosts() {
    this.loading = true;
    this.postService.getPosts().pipe(first()).subscribe(res => {
      this.posts = res;
      this.loading = false;
      // this.alertService.success("Loaded", false, 2000);
    }, err => {

    });
  }

  deletePost(postId) {
    this.postService.deletePost(postId).pipe(first()).subscribe(res => {
      this.loadPosts();
    }, err => {
      this.alertService.error("Could not delete the post", false, 3000);
    });
  }

  deleteUser(id: number) {
    this.userService.delete(id).pipe(first()).subscribe(() => {
      this.loadAllUsers()
    });
  }

  private loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
    });
  }
}