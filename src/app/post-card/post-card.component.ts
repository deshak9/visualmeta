import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";

@Component({
  selector: 'post-card',
  templateUrl: 'post-card.component.html', styleUrls: ['post-card.component.scss']
})
export class PostCardComponent implements OnInit {
  @Input() post;
  @Output() deletePostEmitter: EventEmitter<any> = new EventEmitter();
  isDeleting = false;

  constructor() {

  }

  ngOnInit(): void {
  }

  deletePost() {
    this.isDeleting = true;
    this.deletePostEmitter.emit(this.post.id);
  }

}