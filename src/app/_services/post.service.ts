import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Post} from "../_models/post";

@Injectable()
export class PostService {
  constructor(private http: HttpClient) {
  }

  addPost(post: Post) {
    return this.http.post<any>(`api/posts/add`, post);
  }

  getPosts() {
    return this.http.get<Post[]>(`api/posts`);
  }

  deletePost(id) {
    return this.http.delete<Post[]>(`api/posts/${id}`);
  }
}
