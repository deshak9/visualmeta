import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {PostService} from "../_services/post.service";
import {Post} from "../_models/post";

@Component({
  templateUrl: 'write-post.component.html',
  styleUrls: ['write-post.component.scss']
})

export class WritePostComponent implements OnInit {
  newPost: FormGroup;
  submitted = false;
  loading = false;

  toastOptions = {
    showClose: false,
    timeout: 5000,
    theme: 'default'
  };

  constructor(private formBuilder: FormBuilder, private router: Router, private postService: PostService) {

  }

  ngOnInit() {
    this.newPost = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
    // this.ts.success(Object.assign({title: 'Alert Subscription Deleted'}, this.toastOptions));
  }

  get f() {
    return this.newPost.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.newPost.invalid) {
      return;
    }

    this.loading = true;
    this.router.navigate(['/']);
    let post: Post = new Post();
    post.title = this.f.title.value;
    post.content = this.f.content.value;
    this.postService.addPost(post).subscribe(
      res => {
      },
      err => {
      }
    );
    /*this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });*/
  }
}